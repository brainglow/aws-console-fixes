// replace the text of the navigation shortcuts with their codes
// and put the text itself on as the title, so a hover will still be useful

// not using these at the moment, but we could look up the text here instead of simply showing the code
var cutmaps = { "s3": "EssThree", "sns": "SNS", "ddb": "DynDB", "sqs": "SQS", "imex": "SnoBol" };

function relabel_shortcuts() {
  let bar = document.querySelector("#nav-shortcutBar");
  Array.prototype.slice.call(bar.querySelectorAll(".service-link"))
    .forEach(function(el) {
      let id = el.dataset.serviceId;
      let sl = el.querySelector(".service-label");
      el.title = sl.innerHTML;
      sl.innerHTML = id;
      // sl.innerHTML = cutmaps[id];
    });
}

relabel_shortcuts();
